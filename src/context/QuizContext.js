import { createContext, useContext, useEffect, useReducer } from "react";

const QuizContext = createContext();

const SEC_PER_QUESTIONS = 20;

const initialState = {
  questions: [],
  status: "loading",
  index: 0,
  point: 0,
  answer: null,
  secondsRemaining: 10,
};

function reducer(state, action) {
  switch (action.type) {
    case "dataRecieve":
      return {
        ...state,
        questions: action.payload,
        status: "ready",
      };
    case "error":
      return {
        ...state,
        status: "error",
      };
    case "start":
      return {
        ...state,
        status: "active",
        secondsRemaining: state.questions.length * SEC_PER_QUESTIONS,
      };
    case "newAnswer":
      const question = state.questions.at(state.index);
      console.log(question);
      console.log(action.payload);
      return {
        ...state,
        answer: action.payload,
        point:
          action.payload == question.correctOption
            ? state.point + question.points
            : state.point + 0,
      };
    case "nextQuestion":
      return {
        ...state,
        index: state.index + 1,
        answer: null,
      };
    case "finished": {
      return { ...state, status: "finish" };
    }
    case "restart":
      return {
        ...initialState,
        questions: state.questions,
        status: "ready",
      };
    case "tick":
      return {
        ...state,
        secondsRemaining: state.secondsRemaining - 1,
        status: state.secondsRemaining == 0 ? "finish" : state.status,
      };
    default:
      throw new Error("This action is undefined");
  }
}

function QuizProvider({ children }) {
  const [
    { questions, status, index, point, answer, secondsRemaining },
    dispatch,
  ] = useReducer(reducer, initialState);

  let numberOfQuestions = questions.length;
  const maxPossiblePoints = questions.reduce(
    (prev, cur) => prev + cur.points,
    0
  );
  useEffect(() => {
    fetch("http://localhost:8888/questions")
      .then((res) => res.json())
      .then((data) => dispatch({ type: "dataRecieve", payload: data }))
      .catch((err) => dispatch({ type: "error" }));
  }, []);
  return (
    <QuizContext.Provider
      value={{
        status,
        questions,
        answer,
        index,
        point,
        secondsRemaining,
        numberOfQuestions,
        maxPossiblePoints,
        dispatch,
      }}
    >
      {children}
    </QuizContext.Provider>
  );
}

function useQuiz() {
  return useContext(QuizContext);
}

export { QuizProvider, useQuiz };
