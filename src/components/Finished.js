import React from "react";
import { useQuiz } from "../context/QuizContext";

const Finished = () => {
  const { point, maxPossiblePoints, dispatch } = useQuiz();
  return (
    <>
      <div>
        <div className="result p-4">
          <p>
            Finished with {point} out of {maxPossiblePoints} points !
          </p>{" "}
        </div>
        <button
          onClick={() => dispatch({ type: "restart" })}
          className="btn btn-ui"
        >
          Restart
        </button>
      </div>
    </>
  );
};

export default Finished;
