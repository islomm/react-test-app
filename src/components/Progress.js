import React from "react";
import { useQuiz } from "../context/QuizContext";

const Progress = () => {
  const { index, numberOfQuestions, point, maxPossiblePoints } = useQuiz();
  return (
    <>
      <header className="w-50">
        <progress max={numberOfQuestions} value={index} />
        <p>
          Question <strong>{index + 1}</strong> / {numberOfQuestions}{" "}
        </p>
        <p>
          <strong>
            {point} / {maxPossiblePoints}
          </strong>{" "}
        </p>
      </header>
    </>
  );
};

export default Progress;
