function Header() {
  return (
    <header className="app-header">
      <img src="photo_2023-11-03_16-15-57.jpg" alt="React logo" />
      <h1>The React Quiz</h1>
    </header>
  );
}

export default Header;
