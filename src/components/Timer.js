import React, { useEffect } from "react";
import { useQuiz } from "../context/QuizContext";

const Timer = () => {
  const { secondsRemaining, dispatch } = useQuiz();
  let mins = Math.floor(secondsRemaining / 60);
  let sec = secondsRemaining % 60;
  useEffect(() => {
    let id = setInterval(() => {
      dispatch({ type: "tick" });
    }, 1000);
    return () => {
      clearInterval(id);
    };
  }, [dispatch]);

  return (
    <h2 className="timer">
      {mins} : {sec}
    </h2>
  );
};

export default Timer;
