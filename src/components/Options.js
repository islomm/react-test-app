import React from "react";
import { useQuiz } from "../context/QuizContext";

const Options = ({ question }) => {
  const { dispatch, answer } = useQuiz();
  const hasAnswer = answer != null;
  return (
    <div className="options">
      {question?.options?.map((option, index) => (
        <button
          onClick={() => dispatch({ type: "newAnswer", payload: index })}
          className={`btn btn-option ${index == answer ? "answer" : ""} ${
            hasAnswer
              ? index == question.correctOption
                ? "correct"
                : "wrong"
              : ""
          }`}
          key={option}
          disabled={answer !== null}
        >
          {option}
        </button>
      ))}
    </div>
  );
};

export default Options;
