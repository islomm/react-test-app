import React from "react";
import Options from "./Options";
import { useQuiz } from "../context/QuizContext";

const Questions = () => {
  const { questions, index } = useQuiz();
  const question = questions[index];
  return (
    <div>
      <h4>{question.question} </h4>
      <div>
        <Options question={question} />
      </div>
    </div>
  );
};

export default Questions;
