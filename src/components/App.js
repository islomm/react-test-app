import Header from "./Header";
import Main from "./Main";
import Loader from "./Loader";
import StartScreen from "../StartScreen";
import Error from "./Error";
import Questions from "./Questions";
import NextButton from "./NextButton";
import Finished from "./Finished";
import Progress from "./Progress";
import Timer from "./Timer";
import { useQuiz } from "../context/QuizContext";

function App() {
  const { status } = useQuiz();
  console.log(status);
  return (
    <>
      <div className="app">
        <Header />
        <Main>
          {status == "loading" && <Loader />}
          {status == "error" && <Error />}
          {status == "ready" && <StartScreen />}
          {status == "active" && (
            <>
              <Progress />
              <Questions />
              <Timer />
              <NextButton />
            </>
          )}
          {status == "finish" && <Finished />}
        </Main>
      </div>
    </>
  );
}

export default App;
