import React from "react";
import { useQuiz } from "../context/QuizContext";

const NextButton = () => {
  const { answer, numberOfQuestions, dispatch, index } = useQuiz();
  if (answer == null) {
    return null;
  } else if (index + 1 == numberOfQuestions) {
    return (
      <button
        onClick={() => dispatch({ type: "finished" })}
        className="btn btn-ui"
      >
        Finish
      </button>
    );
  } else {
    return (
      <button
        onClick={() => dispatch({ type: "nextQuestion" })}
        className="btn btn-ui"
      >
        next
      </button>
    );
  }
};

export default NextButton;
