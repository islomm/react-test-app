import React from "react";
import { useQuiz } from "./context/QuizContext";

const StartScreen = () => {
  const { questions, dispatch } = useQuiz();
  // console.log("startscreen");
  // console.log(questions, dispatch);
  return (
    <div className="start">
      <h2>Welcome to React Quiz!</h2>
      <h3>{questions.length} : questions to test your React mastery</h3>
      <button
        className="btn btn-ui"
        onClick={() => dispatch({ type: "start" })}
      >
        Start Quiz
      </button>
    </div>
  );
};

export default StartScreen;
